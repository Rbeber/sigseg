# sigseg

Vitti, A., 2012. Sigseg: a tool for the detection of position and velocity discontinuities in geodetic time-series. GPS solutions, 16(3), pp.405-410.
