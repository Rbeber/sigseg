Table of contents

A) COMPILE
    A.1 RUNNING (local installation)
B) CLEAN
C) INSTALL
   C.1 RUNNING (system installation)
D) UNINSTALL
E) DOCUMENTATION



## A) COMPILE ##

To compile the "sigseg" program type:
make

After compilation, the resulting code is stored in the directory:
./dist

A.1 RUNNING (local installation)
    To run "sigseg" type the following commands:
    export LD_LIBRARY_PATH=./dist/lib
    ./dist/bin/sigseg <path/data_file> <path/prm_file>
    where <data_file> is the relative or absolute path to the input file and 
    <prm_file> is the relative or absolute path to the parameters' file. 
    See the DOCUMENTATION for more details.



## B) CLEAN ##

To delete all files created by 'make' type:
make clean



## C) INSTALL ##

If you want a system-wise installation you can:
  - copy the executable file "sigseg" to /usr/local/bin
    e.g., sudo cp ./dist/bin/sigseg /usr/local/bin

  - copy the include file "libseg1d.h" to /usr/local/include
    e.g., sudo cp ./dist/include/libseg1d.h /usr/local/include

  - copy the library file "libseg1d.so" to /usr/local/lib
    e.g., sudo cp ./dist/include/libseg1d.so /usr/local/lib

  - update the dynamic linker (ld) run-time links and cache
    e.g., sudo ldconfig

Different system folders can be chosen.


C.1 RUNNING (system installation)
    To run "sigseg" type the following commands:
    sigseg <path/data_file> <path/prm_file>
    where <data_file> is the relative or absolute path to the input file and 
    <prm_file> is the relative or absolute path to the parameters' file. 
    See the content of the "doc" folder for more details.



## D) UNINSTALL ##

To delete a system installation remove the files "sigseg", "libseg1d.h" and "libseg1d.so".
    E.g., sudo rm /usr/local/bin/sigseg /usr/local/include/libseg1d.h /usr/local/lib/libseg1d.so
          sudo ldconfig



## E) DOCUMENTATION ##

See the content of the "doc" folder.

