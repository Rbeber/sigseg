/* libseg1d.c */

typedef struct
{
	/* 2 indexes SUM_ij
	 *
	 *
	 * (L=left, C=center, R=right)
	 *
	 * 0<i<n --> 3 combinations, n-2 points
	 *
	 * 	  i	 +
	 * 	        /|\
	 *             / | \
	 *        j   +  +  +
	 *           -1  0  +1
	 *            L  C   R
	 *
	 * i=0 --> 2 combinations, 1 point
	 *
	 *	  i	 +
	 * 	         |\
	 *               | \
	 *        j      +  +
	 *               0  +1
	 *               C   R
	 *
	 *
	 * i=n  --> 2 combinations, 1 point
	 *
	 *	  i      +
	 * 	        /|
	 *             / |
	 *        j   +  +
	 *           -1  0
	 *            L  C  
	 *
	*/ 
	double *pp; 		       /* beta & (alpha-beta) & (u-g)^2 */
	double *ppd; 		       /* beta & (alpha-beta) */
	double *f11, *f12, *f22;       /* (u-g)^2 */
	double *pf1, *pf2; 	       /* (u-g)^2 */
	double *f11dd, *f12dd, *f22dd; /* lambda_epsilon */
	
	/* 4 indexes SUM_ijhk
	 * 
	 * (L=left, C=center, R=right)
	 * 
	 * 0<i<n --> 15 combinations, n-1 points
	 *
	 *   i		  +	  +	  +	  +	  +	  +	  +	 +	+	+	+	+	+	+	+
	 *		 /	 /	 /	 /	  |	  |	  |	 |	|	|	|	 \	 \	 \	 \
	 *   j		+	+	+	+	  +	  +	  +	 +	+	+	+	  +	  +	  +	  +
	 *		|	|	 \	 \	 /	 /	  |	 |	|	 \	 \	 /	 /	  |	  |
	 *   h		+	+	  +	  +	*	+	  +	 +	+	  +	  +	+	+	  +	  +
	 *		|	 \	 /	  |	|	 \	 / 	 |	 \	 /	  |	|	 \	 /	  |
	 *   k 		+	  +	+	  +	+	  +	+	 +	  +	+	  +	+	  +	+	  +
	 *      	L C	L C	L C	L C	L C	L C	L C	 C	C R	C R	C R	C R	C R	C R	C R	
	 *
	 *		 1	 2	 3	 4	 5	 6	 7	 8	 9	 10	 11	 12	 13	 14	 15
	 *
	 * i=0   --> 8 combinations (8 to 15, see the schema above), 1 point
	 *
	 * i=n   --> 8 combinations (1 to 8, see the schema above), 1 point
	 *
	*/
	double *ppf11d, *ppf12d, *ppf22d;    /*mu_epsilon*/
	double *ppf11dd, *ppf12dd, *ppf22dd; /*first 3 SUMs --- gamma*/
}
shp_fun_int; /* Shape functions integrals */


typedef struct
{
	int *i;
	int *j;
	int *h;
	int *k;
}
pos_idx_jhk; /* Given the shape function combination it gives the position indexes for the three indexes summations */


typedef struct
{
	double *u1;  /* The function u that approximates the given data */
	double *u2;  /* The first derivative of the funtion u */
	double *s;   /* The discontinuity function s */
	double *sgm; /* The discntinuity function sigma */
}
appx_u_s_sigma; /* The functional's functions */


typedef struct
{
	double alpha;
	double beta;
	double epsilon;
	double gamma;       /* gamma */
	double lambda_eps;  /* delta_epsilon */
	double mu_eps;      /* mu_epsilon */
}
func_params; /* The functional parameters (or weights). */


typedef struct
{	
	int *j; /* Combination re-indexing for j fix sommatories */
	int *h; /* Combination re-indexing for h fix sommatories */
	int *k; /* Combination re-indexing for k fix sommatories */
}
combs; /* True combinations for Sd_4 terms 
        * Combinations re-indexing is not needed for sommatories with fixed index "i" 
	* since the original combinations can be used */


/* Set up the shape functions' integrals and the position and combination indexes */
int setup(shp_fun_int *, int, pos_idx_jhk *);


/* Minimization of the entire functioal (using the following functions) */
int minimize(double *, int, func_params, appx_u_s_sigma *, shp_fun_int *, pos_idx_jhk, double *);

/* Minimizations of the functional's functions */
int minimize_u1(double *, int, func_params, appx_u_s_sigma *, shp_fun_int *, pos_idx_jhk, combs, int *, double *);
int minimize_u2(double *, int, func_params, appx_u_s_sigma *, shp_fun_int *, pos_idx_jhk, combs, int *);
int minimize_s(double *, int, func_params, appx_u_s_sigma *, shp_fun_int *, pos_idx_jhk, combs, int *);
int minimize_sgm(double *, int, func_params, appx_u_s_sigma *, shp_fun_int *, pos_idx_jhk, combs, int *);

