ifndef LIB_OBJS
LIB_OBJS = $(subst .c,.o,$(wildcard *.c))
endif

ALL_LIB_OBJS := $(foreach obj,$(LIB_OBJS),$(OBJDIR)/$(obj))

SHLIB_NAME = $(LIB_NAME)
SHLIB_OBJS = $(ALL_LIB_OBJS)

SHLIB = $(DIST_LIBDIR)/$(SHLIB_PREFIX)$(SHLIB_NAME)$(SHLIB_SUFFIX)

CFLAGS += $(SHLIB_CFLAGS)
LDFLAGS += $(SHLIB_LDFLAGS)

$(SHLIB): $(SHLIB_OBJS)
	$(CC_SHLIB) -o $@ $(LDFLAGS) $^

shlib: $(SHLIB)

