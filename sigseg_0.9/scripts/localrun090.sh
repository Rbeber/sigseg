#!/bin/bash
#$1: D=data input data file (with full or relative path)
#$2: P=prm input parameters file (with full or relative path)
#$3: K=key character, e.g., A
#          this is useful when different input data files and/or input parameter files are used


export LD_LIBRARY_PATH=$PWD/sigseg_0.9/dist/lib

N=$(wc -l $1 | awk -F' ' ' {print $1}')

$PWD/sigseg_0.9/dist/bin/sigseg $1 $2 | tee /tmp/aa$3.dat
head -$N /tmp/aa$3.dat > /tmp/a$3.dat 
tail -2 /tmp/aa$3.dat | head -1 > /tmp/p$3.dat
