#!/bin/bash
#$1: D=T_data input two columns data file (with full or relative path)
#$2: P=prm input parameters file (with full or relative path)
#$3: K=key character, e.g., A
#          this is useful when different input data files and/or input parameter files are used


awk -F ' ' '{print $2}' $1 > /tmp/D.dat
awk -F ' ' '{print $1}' $1 > /tmp/T.dat

export LD_LIBRARY_PATH=$PWD/dist/lib

N=$(wc -l $1 | awk -F' ' ' {print $1}')

$PWD/dist/bin/sigseg /tmp/D.dat $2 | tee /tmp/aa$3.dat
head -$N /tmp/aa$3.dat > /tmp/a$3.dat 
tail -2 /tmp/aa$3.dat | head -1 > /tmp/p$3.dat
paste -d " " /tmp/T.dat /tmp/a$3.dat > /tmp/Ta$3.dat
