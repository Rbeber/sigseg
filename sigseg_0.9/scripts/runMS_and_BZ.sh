#!/bin/bash
#$1: D=data input data file (with full or relative path)
#$2: P=parameter for MSb
#$3: P=parameter for BZ
#$4: K=key character, e.g., A
#this is useful when different input data files and/or input
#parameter files are used
#
sh ./scripts/localrun.sh $1 $2 $4
name="MSb_data_$4.dat"
cat /tmp/a$4.dat | awk {'print $2'} > $name
#
sh ./scripts/localrun.sh $name $3 $4
#
sh ./scripts/my_gplot.sh $name $4
