#!/bin/bash
#$1: D=data input data file (with full or relative path)
#$2: P=parameter for MSb
#$3: P=parameter for BZ
#$4: K=key character, e.g., A
#this is useful when different input data files and/or input
#parameter files are used
#
sh ./sigseg_0.91/scripts/localrun091.sh $1 $2 $4
name="MSb_data_$4.dat"
cat /tmp/a$4.dat | awk {'print $2'} > /tmp/$name
#
sh ./sigseg_0.9/scripts/localrun090.sh /tmp/$name $3 BZ$4
#
sh ./sigseg_0.9/scripts/my_gplot.sh /tmp/$name BZ$4
