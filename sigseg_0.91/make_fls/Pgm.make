ifndef PGM_OBJS
PGM_OBJS = $(subst .c,.o,$(wildcard *.c))
endif

ALL_PGM_OBJS := $(foreach obj,$(PGM_OBJS),$(OBJDIR)/$(obj))

PROG = $(PGM_BINDIR)/$(PGM_NAME)

cmd: $(PROG)

$(PROG): $(ALL_PGM_OBJS) $(DEPENDENCIES)
	$(CC) $(LDFLAGS) -o $@ $(ALL_PGM_OBJS) $(LIBES) $(MATHLIB)
