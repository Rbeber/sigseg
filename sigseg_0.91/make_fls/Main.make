CC = gcc
CC_SHLIB = gcc -shared

INSTALL = /usr/bin/install -c

HOME_PATH = $(PWD)

DIST_DIR = $(HOME_PATH)/dist
DIST_INCDIR = $(DIST_DIR)/include
DIST_LIBDIR = $(DIST_DIR)/lib

CFLAGS1 = -g -O2
WARN_ME = -Wall
COMPILE_FLAGS = $(CFLAGS1) $(WARN_ME)
CFLAGS = -I$(DIST_INCDIR) -I/home/matteo/localib/gsl/include $(COMPILE_FLAGS)

LINK_FLAGS = -Wl,--export-dynamic
LD_SEARCH_FLAGS = -Wl,-rpath-link,$(DIST_LIBDIR)
LDFLAGS = -L$(DIST_LIBDIR) -L/home/matteo/localib/gsl/lib $(LINK_FLAGS) $(LD_SEARCH_FLAGS)

MATHLIB = -lgsl -lgslcblas -lm 

OBJDIR = OBJS

PGM_NAME = sigseg
PGM_BINDIR = $(DIST_DIR)/bin

LIB_NAME = seg1d
SEG1DLIB = -l$(LIB_NAME)
SHLIB_PREFIX = lib
SHLIB_SUFFIX = .so
SEG1DDEP = $(DIST_LIBDIR)/$(SHLIB_PREFIX)$(LIB_NAME)$(SHLIB_SUFFIX)
SHLIB_CFLAGS = -fPIC
SHLIB_LDFLAGS =

