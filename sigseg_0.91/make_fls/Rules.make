ifndef LOCAL_HEADERS
LOCAL_HEADERS = $(wildcard *.h)
endif

# default CC rules
$(OBJDIR)/%.o : %.c $(LOCAL_HEADERS) 
	@test -d $(OBJDIR) || mkdir $(OBJDIR)	
	$(CC) $(CFLAGS) $(INC) \
		-o $(OBJDIR)/$*.o -c $*.c

