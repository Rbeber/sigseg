## makefile extension ##

CC = gcc

INSTALL = /usr/bin/install -c

INST_DIR = /usr/local/sigseg

LD_SEARCH_FLAGS = -Wl,-rpath-link,$(LIB_RUNTIME_DIR)

LIB_RUNTIME_DIR = $(ARCH_LIBDIR)

SHLIB_PREFIX = lib
SHLIB_LD = gcc -shared
SHLIB_LD_LIBS = ${LIBS} #USATO???#
SHLIB_CFLAGS = -fPIC
SHLIB_SUFFIX = .so

HOME_PATH = $(PWD)

CFLAGS1 = -g -O2
WARN_ME = -Wall
INCLUDE_DIRS = -I/home/ran/gsl/include

COMPILE_FLAGS = $(CFLAGS1) $(WARN_ME) $(INCLUDE_DIRS)
LINK_FLAGS = -Wl,--export-dynamic

MATHLIB = -lgsl -lgslcblas -lm


#---#

ARCH_DISTDIR = $(HOME_PATH)/bin
ARCH_BIN = $(ARCH_DISTDIR)

PGM_INCDIR = $(HOME_PATH)/include
ARCH_INCDIR = $(ARCH_DISTDIR)/include

PGM_INC = -I$(PGM_INCDIR)
ARCH_INC = -I$(ARCH_INCDIR)
INC = $(ARCH_INC) -I/home/ran/gsl/include


PGM_LIBDIR = $(HOME_PATH)/seglib1d
ARCH_LIBDIR = $(ARCH_DISTDIR)/lib

ARCH_LIBPATH = -L$(ARCH_LIBDIR) -L/home/ran/gsl/lib
LIBPATH = $(ARCH_LIBPATH)

OBJDIR = OBJS

CFLAGS = $(INC) $(COMPILE_FLAGS)
LDFLAGS = $(LIBPATH) $(LINK_FLAGS) $(LD_SEARCH_FLAGS)

SEG1D_LIBNAME = seg1d
SEG1DLIB = -l$(SEG1D_LIBNAME)
SEG1DDEP = $(ARCH_LIBDIR)/$(SHLIB_PREFIX)$(SEG1D_LIBNAME)$(SHLIB_SUFFIX)





###

subdirs:
	@list='$(SUBDIRS)'; \
        for subdir in $$list; do \
            echo $$subdir ; \
            $(MAKE) default -C $$subdir || echo $$subdir >> $(TOP_PATH)/error.log; \
        done




###PROGRAM'S###

ifndef PGM_OBJS
PGM_OBJS = $(subst .c,.o,$(wildcard *.c))
endif

ARCH_PGM_OBJS := $(foreach obj,$(PGM_OBJS),$(OBJDIR)/$(obj))

###---### #duplicato#

ifndef LOCAL_HEADERS
LOCAL_HEADERS = $(wildcard *.h)
endif

# default cc rules
$(OBJDIR)/%.o : %.c $(LOCAL_HEADERS) 
	@test -d $(OBJDIR) || mkdir $(OBJDIR)	
	$(CC) $(CFLAGS) $(INC) \
		-o $(OBJDIR)/$*.o -c $*.c

PROG = $(ARCH_BIN)/$(PGM)

cmd: $(PROG)

$(PROG): $(ARCH_PGM_OBJS) $(DEPENDENCIES)
	$(CC) $(LDFLAGS) -o $@ $(ARCH_PGM_OBJS) $(LIBES) $(MATHLIB)




### LIBRARY'S ### 


ifndef LIB_OBJS
LIB_OBJS = $(subst .c,.o,$(wildcard *.c))
endif

ARCH_LIB_OBJS := $(foreach obj,$(LIB_OBJS),$(OBJDIR)/$(obj))

SHLIB_NAME = $(LIB_NAME)
SHLIB_OBJS = $(ARCH_LIB_OBJS)

#---#

ifndef LOCAL_HEADERS
LOCAL_HEADERS = $(wildcard *.h)
endif

# default cc rules
$(OBJDIR)/%.o : %.c $(LOCAL_HEADERS) 
	@test -d $(OBJDIR) || mkdir $(OBJDIR)	
	$(CC) $(CFLAGS) $(INC) \
		-o $(OBJDIR)/$*.o -c $*.c


#---#


SHLIB = $(ARCH_LIBDIR)/$(SHLIB_PREFIX)$(SHLIB_NAME)$(SHLIB_SUFFIX)

CFLAGS += $(SHLIB_CFLAGS)
LDFLAGS += $(SHLIB_LDFLAGS)

$(SHLIB): $(SHLIB_OBJS)
	$(SHLIB_LD) -o $@ $(LDFLAGS) $^

shlib: $(SHLIB)

#lib: shlib

