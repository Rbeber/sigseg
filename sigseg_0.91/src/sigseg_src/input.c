/*******************************************************************************
 *
 * PROGRAM:	 sigseg
 *		 
 * FILE: 	 input.c
 *
 * AUTHOR:       Alfonso Vitti - University of Trento - Italy
 *                               alfonso.vitti AT ing.unitn.it
 *
 * PURPOSE:      Read the input data file and get its number of lines.
 *
 * COPYRIGHT:    (C) 2009 Alfonso Vitti
 *
 *               This program is free software under the GNU General Public
 *               License (>=v2). See file COPYING for details.
 *
 ******************************************************************************/

#include <stdio.h>
#include <stdlib.h>

#define LINELENGTH 80


/* Get the number of line of the input file, i.e., the number of points. */
int get_nol(char *input_name)
{
	FILE *fp;
	char line[LINELENGTH];
	int nol=0;

	if ((fp = fopen(input_name, "r")) == NULL)
		return (-1);

	while (fgets(line, LINELENGTH, fp) != NULL)
		++nol;

	fclose(fp);

	return (nol);
}


/* Read the input file */
int read_input(char *input_name, int nol, double *data)
{
	FILE *fp;
	char line[LINELENGTH];
	int i;

	if ((fp = fopen(input_name, "r")) == NULL)
		return (-1);

	for (i=0 ; i < nol; i++)
	{
		fgets(line, LINELENGTH, fp);
		data[i] = atof(line);
	}

	fclose(fp);

	return (0);
}

