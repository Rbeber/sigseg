/*******************************************************************************
 *
 * PROGRAM:	 sigseg
 *		 
 * FILE: 	 main.c
 *
 * AUTHOR:       Alfonso Vitti - University of Trento - Italy
 *                               alfonso.vitti AT ing.unitn.it
 *
 * PURPOSE:      To minimize numerically the Mumford-Shah or the 
 *		 Blake-Zisserman functional in order to obtain a variational
 *		 segmentation of a 1D data set.
 *               See the conttent of the "docs" folder for documentation.
 *
 * NOTE:	 The program relies on the "libseg1d" library.
 *
 * COPYRIGHT:    (C) 2009 Alfonso Vitti
 *
 *               This program is free software under the GNU General Public
 *               License (>=v2). See file COPYING for details.
 *
 ******************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <libseg1d.h>
#include <gsl/gsl_math.h>
#include <gsl/gsl_movstat.h>
#include <gsl/gsl_vector.h>
#include "local_proto.h"

#define MAX(A,B) (A-B)>0?A:B
#define MIN(A,B) (A-B)<0?A:B

#define LINEL 80


int main(int argc, char *argv[]) 
{
	int arg=1;
	char *input_name; /* Input file name */
	int np=0;         /* Number of points in the input file, i.e. its number of lines */
	int n=0;          /* Node number (starts from 0), i.e. (n = number of point -1) */
	double *g_in;     /* Input signal, used also for the fem approximation */
	shp_fun_int sfi;  /* Data type defined in sigseg.h (it stores the shape functions integrals */

	appx_u_s_sigma apx;
	pos_idx_jhk pos;  /* See definition in libseg1d.h */
	func_params prm;

	int step=0;        /* Iterations counter */
	int mx_iter=0;     /* Max number of iterations */
	double mx_diff=0.; /* Max difference over the entire data-set between two iterative solutions */
	double tol=0.;     /* Threshold to stop the iterative loop: 
        	            * when "mx_diff" becomes smaller than "tol" the iteration is stopped */

	FILE *fp;
	char *paramfile;
	double *parametri;
	int i,r;


	/* Check input parameters */
	if (argc <3)
	{
		fprintf(stderr, "\n");
		fprintf(stderr, "  ERROR: input file(s) missing\n");
		fprintf(stderr, "  command syntax:\n\n");
		fprintf(stderr, "  sigseg <path/data_file> <path/prm_file>\n\n");
		fprintf(stderr, "  <data_file> is the path to the input file\n");
		fprintf(stderr, "  <prm_file>  is the path to the parameters file\n\n");
		return (EXIT_FAILURE);
	}
	else if (argc >3)
	{
		fprintf(stderr, "\n");
		fprintf(stderr, "  ERROR: input to many input parameters\n");
		fprintf(stderr, "  command syntax:\n\n");
		fprintf(stderr, "  sigseg <path/data_file> <path/prm_file>\n\n");
		fprintf(stderr, "  <data_file> is the path to the input file\n");
		fprintf(stderr, "  <prm_file>  is the path to the parameters file\n\n");
		return (EXIT_FAILURE);
	}
	else
	{
		input_name = argv[arg++];
		paramfile = argv[arg];
	}


	/* Read the parameters file and store parameters' values */
	/* !!!___THIS PART OF THE CODE SHOULD BE MOVE TO THE "input.c" FILE___!!! */
	if ((fp = fopen(paramfile, "r")) == NULL)
	{
		fprintf(stderr, "\n");
		fprintf(stderr, "  ERROR: can't open parameters file \"%s\"\n", paramfile);
		fprintf(stderr, "  Does the file exist? is it readable?\n\n");
		return (EXIT_FAILURE);
	}
	else
	{
		parametri=malloc(29 * sizeof(double));

		for (r=0 ; r <=28 ; r++)
		{
			char line[80];
			fgets(line, 80, fp);
			parametri[r] = atof(line);
		}

		fclose(fp);
	}

	prm.alpha = parametri[1];
	prm.beta = parametri[4];
	prm.epsilon = parametri[7];
	prm.gamma = parametri[10];
	prm.mu_eps = parametri[13];
	prm.lambda_eps = parametri[16];
	prm.tau = parametri[19];
	prm.var_loc_box = (int)parametri[22];
	tol = parametri[25];
	mx_iter = (int)parametri[28];

	free(parametri);
	/*printf("#prm: ##alpha= %g ##beta= %g ##epsilon= %g ##gamma= %g ##mu_eps= %g  ##lambda_eps= %g ##tau= %g  ##var_box= %d ##tol= %g ##n_it= %d\n", prm.alpha, prm.beta, prm.epsilon, prm.gamma, prm.mu_eps, prm.lambda_eps, prm.tau, prm.var_loc_box, tol, mx_iter);*/

	if((tol <= 0.) && (mx_iter <= 0))
	{
		fprintf(stderr, "\n");
		fprintf(stderr, "  ERROR: tol and mx_iter can't both be set to zero\n");
		fprintf(stderr, "  Check the input parameter file \"%s\"\n\n", paramfile);
		return (EXIT_FAILURE);
	}


	/* Get number of lines of the input file, i.e., number of point.
	 * ["get_nol" function defined in the input.c file]
	 */
	if((np	= get_nol(input_name)) == -1)
	{
		fprintf(stderr, "\n");
		fprintf(stderr, "  ERROR: can't open input file \"%s\"\n", input_name);
		fprintf(stderr, "  Does the file exist? is it readable?\n\n");
		return (EXIT_FAILURE);
	}

	n = np - 1;	


	/* Memory allocation for input data 
	 * ["g_in" function defined in the input.c file]
	 */
	if((g_in = malloc(np * sizeof(double))) == NULL)
	{
		fprintf(stderr, "\n");
		fprintf(stderr, "  ERROR: data memory allocation failed.\n\n");
		return (EXIT_FAILURE);
	}

	/* Read input data g_in [function "" s defined in ] */
	if(read_input(input_name, np, g_in) == -1)
	{
		fprintf(stderr, "\n");
		fprintf(stderr, "  ERROR: can't open input file \"%s\"\n", input_name);
		fprintf(stderr, "  Has the file been removed?\n\n");
		return (EXIT_FAILURE);
	}


	/* 
	 * Memory allocation for shape functions and indexes
	 *
	 * 2 indexes shape functions -->  5 combinations: -2|-1| 0|+1|+2
	 *                                           idx:  0  1  2  3  4 
	 *
	 * 4 indexes shape functions --> 17 combinations: -8|-7|-6|-5|-4|-3|-2|-1| 0|+1|+2|+3|+4|+5|+6|+7|+8
	 *                                           idx:  0  1  2  3  4  5  6  7  8  9 10 11 12 13 14 15 16
	 *
	 *
	 * See libseg1D.h and the "docs" folder for explanation.
	 */
	sfi.pp = malloc(5 * sizeof(double));
	sfi.ppd = malloc(5 * sizeof(double));
	sfi.f11 = malloc(5 * sizeof(double));
	sfi.f12 = malloc(5 * sizeof(double));
	sfi.f22 = malloc(5 * sizeof(double));
	sfi.pf1 = malloc(5 * sizeof(double));
	sfi.pf2 = malloc(5 * sizeof(double));
	sfi.f11dd = malloc(5 * sizeof(double));
	sfi.f12dd = malloc(5 * sizeof(double));
	sfi.f22dd = malloc(5 * sizeof(double));

	sfi.ppf11d = malloc(17 * sizeof(double));
	sfi.ppf12d = malloc(17 * sizeof(double));
	sfi.ppf22d = malloc(17 * sizeof(double));
	sfi.ppf11dd = malloc(17 * sizeof(double));
	sfi.ppf12dd = malloc(17 * sizeof(double));
	sfi.ppf22dd = malloc(17 * sizeof(double));

	pos.i = malloc(17 * sizeof(int));
	pos.j = malloc(17 * sizeof(int));
	pos.h = malloc(17 * sizeof(int));
	pos.k = malloc(17 * sizeof(int));

	for (i = 0; i <= 4; i++)
	{
		sfi.pp[i] = 0.;
		sfi.ppd[i] = 0.; 
		sfi.f11[i] = 0.; 
		sfi.f12[i] = 0.; 
		sfi.f22[i] = 0.; 
		sfi.pf1[i] = 0.; 
		sfi.pf2[i] = 0.; 
		sfi.f11dd[i] = 0.;
		sfi.f12dd[i] = 0.;
		sfi.f22dd[i] = 0.;  
	}

	for (i = 0; i <= 16; i++)
	{
		sfi.ppf11d[i] = 0.;
		sfi.ppf12d[i] = 0.;
		sfi.ppf22d[i] = 0.;
		sfi.ppf11dd[i] = 0.;
		sfi.ppf12dd[i] = 0.;
		sfi.ppf22dd[i] = 0.;

		pos.i[i] = 0;
		pos.j[i] = 0;
		pos.h[i] = 0;
		pos.k[i] = 0;
	}

	apx.u1 = malloc(np * sizeof(double));
	apx.u2 = malloc(np * sizeof(double));
	apx.s = malloc(np * sizeof(double));
	apx.sgm = malloc(np * sizeof(double));
	apx.var = malloc(np * sizeof(double));


	/* Initialization of u (apx.u1), u' (apx.u2), s (apx.s) and sigma (apx.sgm).
	 * A weighted moving average of the data g (g_in) is used to initialize u (apx.u1).
	 * The variable u' (apx.u2) is set to zero.
	 * The variable s and  sigma are set to the value 0.5.
	 * rb here we can use the opportunity of the moving average to compute the local variance var_loc (apx.var)
*/
	apx.u1[0] = (g_in[0]+0.5*g_in[1])/1.5;
	for (i = 1; i < np-1; i++)
	{
		apx.u1[i] = 0.5*(g_in[i]+0.5*(g_in[i-1]+g_in[i+1])); 
	}
	apx.u1[np-1] = (g_in[np-1]+0.5*g_in[np-2])/1.5;

	gsl_vector *x = gsl_vector_alloc(np);

	for (i = 0; i <= np-1; i++)
	{
		apx.u2[i] = 0.;
		apx.s[i] = 0.5;
		apx.sgm[i] = 0.5;
		apx.var[i] = 0.; /* Initilize variance to zero as other variables*/
		gsl_vector_set(x, i, g_in[i]); /*copy input data to a gsl vector*/
	}

	/*Calculate the variance moving average for the extreme of g_in 
	* first the sd on a moving windows is calculated with the library #include <gsl/gsl_movstat.h>
	* in particular the proposed estimator Qn by Rousseeuw & Croux (1993) DOI: 10.1080/01621459.1993.10476408
	* then the sd in converted in variance taking the square
	*/
	
	/* Allocate vectors for sd computation */
	gsl_vector *xQn = gsl_vector_alloc(np);
	gsl_movstat_workspace * w = gsl_movstat_alloc(prm.var_loc_box);		/* window size */

 	/* compute moving statistics */
 	gsl_movstat_Qn(GSL_MOVSTAT_END_TRUNCATE, x, xQn, w); /* here a truncation option has been choosen*/

	/*transfer data back to sigseg format*/
	for (i = 0; i < np; ++i)
	{
	  apx.var[i]=gsl_pow_2(gsl_vector_get(xQn, i));
	}
	gsl_vector_free(x);
	gsl_vector_free(xQn);
	gsl_movstat_free(w);
	

	/* Inizialization of the shape function integrals. 
	 * ["setup" function defined in the seglib1d library] 
	 */
	if (setup(&sfi, n, &pos) != 0)
	{
		fprintf(stderr, "\n");
		fprintf(stderr, "  ERROR: failed to set up function [seglib1d library].\n\n");
		return (EXIT_FAILURE);
	}


	/* Minimize the functional 
	 * ["minimize" function defined in the seglib1d library]
	 */
	/* 1st iteration */
	if (minimize(g_in, np, prm, &apx, &sfi, pos, &mx_diff) != 0)
	{
		fprintf(stderr, "\n");
		fprintf(stderr, "  ERROR: functional minimization failed.\n\n");
		return (EXIT_FAILURE);
	}
	step++;


	/* successive iterations */
	while((mx_diff > tol) && (step < mx_iter))
	{
		mx_diff=0.;
		step++;
		if (minimize(g_in, np, prm, &apx, &sfi, pos, &mx_diff) != 0)
		{
			fprintf(stderr, "\n");
			fprintf(stderr, "  ERROR: functional minimization failed.\n\n");
			return (EXIT_FAILURE);
		}
	}

	/* Print the result on the standard output */
	for (i=0; i<=np-1;i++)
	{
		printf("%.6f %.6f %.6f %.6f %.6f %.6f\n", g_in[i], apx.u1[i], apx.u2[i], apx.s[i], apx.sgm[i], apx.var[i]);
	}
	printf("#\n");
	printf("#STOPED BY: %d (%d) %g (%g)\n", step, mx_iter, mx_diff, tol);
	printf("#\n");
	printf("#prm: ##alpha= %g ##beta= %g ##gamma= %g ##mu_eps= %g ##tau= %g ##var_box= %d ##n_it= %d\n", prm.alpha, prm.beta, prm.gamma, prm.mu_eps, prm.tau, prm.var_loc_box, mx_iter);
	printf("#\n");


	/* Print the result on a text file */
	/* 
	 * This is a placeholder 
	 */


	/* Free the allocated memory */
	free(g_in);

	free(sfi.pp);
	free(sfi.ppd);
	free(sfi.f11);
	free(sfi.f12);
	free(sfi.f22);
	free(sfi.pf1);
	free(sfi.pf2);
	free(sfi.f11dd);
	free(sfi.f12dd);
	free(sfi.f22dd);
           
	free(sfi.ppf11d);
	free(sfi.ppf12d);
	free(sfi.ppf22d);
	free(sfi.ppf11dd);
	free(sfi.ppf12dd);
	free(sfi.ppf22dd);

	free(pos.j);
	free(pos.h);
	free(pos.k);

	free(apx.u1);
	free(apx.u2);
	free(apx.s);
	free(apx.sgm);


	/* Now exit */
	return (EXIT_SUCCESS);
}

