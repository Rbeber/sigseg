#!/bin/bash
#$1: D=data input data file (with full or relative path)
#$2: K=key character, e.g., A
#          this is useful when different input data files and/or input parameter files are used
#$3: R=range for the first derivative plot
R=$3
if [ -z ${R} ]; then R=10
fi 


F=$(basename $1 .dat)
A=$(awk -F' ' '{print $3}' /tmp/p$2.dat)
B=$(awk -F' ' '{print $5}' /tmp/p$2.dat)
G=$(awk -F' ' '{print $7}' /tmp/p$2.dat)
ME=$(awk -F' ' '{print $9}' /tmp/p$2.dat)
IT=$(awk -F' ' '{print $11}' /tmp/p$2.dat)


gnuplot -persist << EOF
set multiplot title "$F \n\n alpha $A | beta $B | gamma $G | mu_eps ${ME} | n_it ${IT}"
set format y "%9.1f"
set size 1,0.33
set origin 0,0.63
set yrange [ -0.2 : +1.2 ]
plot "/tmp/a$2.dat" u 5 t "sgm [u]" w l, "/tmp/a$2.dat" u 4 t "s [u']" w l
set format y "%9.3f"
set size 1,0.3
set origin 0,0.3
set yrange [*:*]
plot "/tmp/a$2.dat" u 1 t "g" w l lt -1 lw 0, "/tmp/a$2.dat" u 2 t "u" w l lt 1 lw 2
set size 1,0.3
set origin 0,0
set yrange [ -${R} : +${R} ]
plot "/tmp/a$2.dat" u 3 t "u'" w l lt 1 lw 0
EOF
